// neural-weather-forecaster project main.go
package main

import (
	"fmt"
	"io/ioutil"
	"os"
	//	"runtime"
	"sync"
	"time"

	"bitbucket.org/codetrasher/neural-weather-forecaster/forecaster"
	//	"github.com/avesanen/neuro"
)

var (
	wd         = new(forecaster.WeatherData)
	wg         sync.WaitGroup
	tstart, tt = time.Now(), time.Now()
)

func main() {

	//	fmt.Printf("CPUs in use: %v\n\n", runtime.GOMAXPROCS(0))

	/**************
	PARSE WEATHER DATA
	***************/
	os.Chdir("data")

	files, _ := ioutil.ReadDir(".")
	wg.Add(len(files))
	//fmt.Printf("N of files: %v\n", forecaster.FilesAmount("data"))

	//s, _ := os.Getwd()
	//fmt.Printf("Curr dir: %s\n\n", s)
	//fmt.Printf("Files: %v\n\n", files)
	for _, f := range files {

		//fmt.Printf("Working on file... %s\n\n", f.Name())
		go func(fn string) {
			defer wg.Done()

			//fmt.Printf("fn: %s\n\n", fn)

			forecaster.ReadData(fn, wd)
		}(f.Name())
	}

	wg.Wait()

	fmt.Printf("Size of DataSet #1: %v\n", len(wd.GetPPDataSet()))
	fmt.Printf("Size of DataSet #2: %v\n", len(wd.GetRRDataSet()))
	fmt.Printf("Size of DataSet #3: %v\n", len(wd.GetTGDataSet()))
	fmt.Printf("Size of DataSet #4: %v\n", len(wd.GetTNDataSet()))
	fmt.Printf("Size of DataSet #5: %v\n", len(wd.GetTXDataSet()))

	/******************************
	TRAIN NEURAL NETWORK AND PREDICT UPCOMING WEATHER FOR EACH SET OF DATA
	*******************************/
	wg.Add(len(files))
	defer wg.Done()
	go forecaster.Predict(forecaster.GetNormalizedMap(wd.GetPPDataSet()), "PP")
	go forecaster.Predict(forecaster.GetNormalizedMap(wd.GetRRDataSet()), "RR")
	go forecaster.Predict(forecaster.GetNormalizedMap(wd.GetTGDataSet()), "TG")
	go forecaster.Predict(forecaster.GetNormalizedMap(wd.GetTNDataSet()), "TN")
	go forecaster.Predict(forecaster.GetNormalizedMap(wd.GetTXDataSet()), "TX")
	wg.Done()

	telapsed := time.Now()

	fmt.Printf("Total execution time: %v.\n", telapsed.Sub(tstart))
	fmt.Println("SUCCESS!\n\n")

	//	timestr, _ := time.Parse("20160101000000", "20160814000000")
	//	fmt.Println("Time format testing: ", timestr)
}
