package forecaster

import (
	"fmt"
	//	"strconv"

	"github.com/avesanen/neuro"
)

const TRAIN_ACCURACY float64 = 0.95

var (
//	pd DataPoint // predicted datapoint
)

func train(ff float64, bp float64, nn *neuro.Network) float64 {
	//	fmt.Printf("Values: %f, %f\n", ff, bp)

	nn.FeedForward([]float64{ff})
	nn.BackPropagation([]float64{bp})
	nn.FeedForward([]float64{ff})

	r := nn.Results()

	//	fmt.Printf("Results: %f\n", r[0])

	return r[0] / bp
}

func Predict(m map[string]float64, id string) {
	dates := SortKeys(m) // sort keys to increasing order

	// Create the neural network for building predicting model
	in, hid, out := 1, 3, 1
	nn := neuro.NewNetwork([]int{in, hid, hid, out})

	fmt.Printf("Neuro address: %v\n", &nn)

	// Start predicting process...
	for i, date := range dates {

		if i < len(dates)-1 { // here happens the actual training

			//			fmt.Printf("training... %d\n", i)

			nextDate := dates[i+1]
			//			var acc float64

			//			fmt.Printf("dates: %s, %s\n", date, nextDate)
			//			fmt.Printf("values: %f, %f\n", m[date], m[nextDate])

			for {
				//				acc =
				//				fmt.Printf("Acc: %f\n", acc)
				if train(m[date], m[nextDate], nn) >= TRAIN_ACCURACY {
					break
				}
			}
		} else { // ...and here we predict the next day's data
			nn.FeedForward([]float64{m[date]})

			r := nn.Results()

			fmt.Printf("Next day's forecast: %f\n", r[0])
		}
	}

	fmt.Printf("Routine %s finished!\n", id)
}
