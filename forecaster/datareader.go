// datareader
package forecaster

import (
	"bufio"
	//	"fmt"
	"log"
	"os"
	"regexp"
	"sort"
	"strconv"
	"strings"
)

const DATA_REGEX string = "\\d{8},\\s*-?\\d+"

func ReadData(fn string, wd *WeatherData) {

	//fmt.Printf("filename: %s, weatherData: %v\n", fn, wd)

	if strings.HasPrefix(fn, "PP_") {
		data, ndata := read(fn)
		for d, v := range data {
			wd.InsertPP(d, v, ndata[d])
		}
	} else if strings.HasPrefix(fn, "RR_") {
		data, ndata := read(fn)
		for d, v := range data {
			wd.InsertRR(d, v, ndata[d])
		}
	} else if strings.HasPrefix(fn, "TG_") {
		data, ndata := read(fn)
		for d, v := range data {
			wd.InsertTG(d, v, ndata[d])
		}
	} else if strings.HasPrefix(fn, "TN_") {
		data, ndata := read(fn)
		for d, v := range data {
			wd.InsertTN(d, v, ndata[d])
		}
	} else if strings.HasPrefix(fn, "TX_") {
		data, ndata := read(fn)
		for d, v := range data {
			wd.InsertTX(d, v, ndata[d])
		}
	} else {
		log.Fatal("Error: File with correct prefix not found.")
	}
}

func read(f string) (m, nm map[string]float64) {

	// Function for normalizing values in the map
	normalize := func(m map[string]float64) (nm map[string]float64) {

		// Sort the map: Make slice for float32 values, sort them and iterate them back to the map.
		var vals []float64
		for _, v := range m {
			vals = append(vals, v)
		}
		sort.Float64s(vals)

		// Get minimum and maximum values
		min, max := vals[0], vals[len(vals)-1]
		min *= 0.0001
		max *= 0.9999

		// Find matching keys, calculate normalized value and replace old value in the original map.
		nm = make(map[string]float64)
		for k, v := range m {
			//			for k2 := range c {
			//			if k1 == k2 {
			nm[k] = (v - min) / (max - min)
			//			}
			//			}
		}

		return
	}

	file, err := os.Open(f)
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	r, _ := regexp.Compile(DATA_REGEX)
	//fmt.Printf("Regex: %v\n", r)
	sc := bufio.NewScanner(file)

	m = make(map[string]float64)
	for sc.Scan() {
		s := sc.Text()
		//fmt.Printf("S = %s\n", s)
		if str := r.FindString(s); str != "" {
			//			fmt.Printf("str: %v\n", str)

			sp := strings.Split(str, ",")

			v, _ := strconv.Atoi(strings.TrimSpace(sp[1]))

			if v != -9999 {
				m[sp[0]] = float64(v)
			}
		}
	}
	nm = normalize(m)

	//	fmt.Printf("Original map after reading: %v\n", m)

	return
}
