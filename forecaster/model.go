// forecaster project model.go
package forecaster

import (
	"sort"
)

type DataPoint struct {
	date       string
	value      float64
	normalized float64
}

type WeatherData struct {
	pp []DataPoint // air pressure
	rr []DataPoint // precipitation
	tg []DataPoint // mean temperature
	tn []DataPoint // minimum temperature
	tx []DataPoint // maximum temperature
}

func (wd *WeatherData) InsertPP(d string, v float64, n float64) {
	dp := DataPoint{d, v, n}
	wd.pp = append(wd.pp, dp)
}

func (wd *WeatherData) InsertRR(d string, v float64, n float64) {
	dp := DataPoint{d, v, n}
	wd.rr = append(wd.rr, dp)
}

func (wd *WeatherData) InsertTG(d string, v float64, n float64) {
	dp := DataPoint{d, v, n}
	wd.tg = append(wd.tg, dp)
}

func (wd *WeatherData) InsertTN(d string, v float64, n float64) {
	dp := DataPoint{d, v, n}
	wd.tn = append(wd.tn, dp)
}

func (wd *WeatherData) InsertTX(d string, v float64, n float64) {
	dp := DataPoint{d, v, n}
	wd.tx = append(wd.tx, dp)
}

func (wd *WeatherData) GetPPDataSet() []DataPoint {
	return wd.pp
}

func (wd *WeatherData) GetRRDataSet() []DataPoint {
	return wd.rr
}

func (wd *WeatherData) GetTGDataSet() []DataPoint {
	return wd.tg
}

func (wd *WeatherData) GetTNDataSet() []DataPoint {
	return wd.tn
}

func (wd *WeatherData) GetTXDataSet() []DataPoint {
	return wd.tx
}

func SortKeys(m map[string]float64) (str []string) {

	for k := range m {
		str = append(str, k)
	}
	sort.Strings(str)

	return
}

func GetNormalizedMap(dp []DataPoint) (m map[string]float64) {

	m = make(map[string]float64)

	for _, d := range dp {
		m[d.date] = d.normalized
	}

	return
}
